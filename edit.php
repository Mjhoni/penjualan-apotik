<?php 
    require_once("koneksi.php");

    $id_obat = $_GET['id_obat'];

    $sql_cari = "SELECT * FROM obat WHERE id_obat ='$id_obat'";
    $query = mysqli_query($koneksi, $sql_cari);
    $result = mysqli_fetch_assoc($query);

    if(isset($_POST['submit'])){
        $id_obat = $_POST['id_obat'];
		$nama_obat = $_POST['nama_obat'];
		$jenis_obat = $_POST['jenis_obat'];
        $stock = $_POST['stock'];
        $harga = $_POST['harga'];
    
        $sql_edit = "UPDATE obat SET nama_obat = '$nama_obat', jenis_obat = 'jenis_obat', stock = '$stock', harga = '$harga' WHERE id_obat= '$id_obat' ";
        mysqli_query($koneksi, $sql_edit);
    
        header("Location:index.php");
    }

?>

<!DOCTYPE html>
<html>
<head>
	<title>TP Basis Data</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="judul">		
		<h1>Apotik Kurnia Jaya</h1>
		<h2>Jalan Palembang-Jambi km.102, Sungai Lilin, Musi Banyuasin, Sumatera Selatan</h2>
	</div>
	
	<br/>
 
	<a href="index.php">Lihat Semua Data</a>
 
	<br/>
	<h3>Silahkan Edit Data Obat:</h3>
	<form action="edit.php" method="POST">		
		<table>
            <tr>
				<td>Id Obat</td>
				<td><input type="number" name="id_obat" value="<?= $result['id_obat']; ?>"></td>					
			</tr>
			<tr>
				<td>Nama Obat</td>
				<td><input type="text" name="nama_obat" value="<?= $result['nama_obat']; ?>"></td>					
			</tr>	
			<tr>
				<td>Jenis Obat</td>
				<td><input type="text" name="jenis_obat" value="<?= $result['jenis_obat']; ?>"></td>					
			</tr>	
			<tr>
				<td>Stok</td>
				<td><input type="number" name="stock" value="<?= $result['stock']; ?>"></td>					
			</tr>	
			<tr>
				<td>Harga</td>
				<td><input type="number" name="harga" value="<?= $result['harga']; ?>"></td>					
			</tr>	
            				
		</table>
        <button name="submit" type="submit">Ubah Data</button>
	</form>
</body>
</html>