<?php 
require_once("koneksi.php");
if(isset($_POST['submit'])){
    $id_obat = $_POST['id_obat'];
    $nama_obat = $_POST['nama_obat'];
    $jenis_obat = $_POST['jenis_obat'];
    $stock = $_POST['stock'];
    $harga= $_POST['harga'];

    $sql_insert = "INSERT INTO obat VALUES('$id_obat', '$nama_obat','$jenis_obat', '$stock', '$harga')";
    mysqli_query($koneksi, $sql_insert);

    header("Location:index.php");
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>TP Basis Data</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="judul">		
		<h1>Apotik Kurnia Jaya</h1>
		<h2>Jalan Palembang-Jambi km.102, Sungai Lilin, Musi Banyuasin, Sumatera Selatan</h2>
	</div>
	
	<br/>
 
	<a href="index.php">Lihat Semua Data Obat</a>
 
	<br/>
	<h3>Silahkan Input data Obat:</h3>
	<form action="input.php" method="POST">		
		<table>
            <tr>
				<td>Id Obat</td>
				<td><input type="number" name="id_obat"></td>					
			</tr>
			<tr>
				<td>Nama Obat</td>
				<td><input type="text" name="nama_obat"></td>					
			</tr>	
			<tr>
				<td>Jenis Obat</td>
				<td><input type="text" name="jenis_obat"></td>					
			</tr>
			<tr>
				<td>Stok</td>
				<td><input type="number" name="stock"></td>					
			</tr>	
			<tr>
				<td>Harga</td>
				<td><input type="number" name="harga"></td>					
			</tr>					
		</table>
        <button name="submit" type="submit">Tambah Data</button>
	</form>
</body>
</html>