<?php

    require_once("koneksi.php");
    $sql_gett = "SELECT * FROM transaksi";
    $query_pjn = mysqli_query($koneksi, $sql_gett);
    $resultss = [];
    while($rows = mysqli_fetch_assoc($query_pjn)){
        $resultss[] = $rows;
    }

?>

<!DOCTYPE html>
<html>
<head>
	<title>TP Basis Data</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <style>
		a:link, a:visited {
  background-color:  #0fbda0;
  color: white;
  padding: 5px 3px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}

a:hover, a:active {
  background-color: red;

}
th{
	padding: 15px 3px;
	background-color:  #0fbda0;
	  
	text-align: center;
  text-decoration: none;
 
}
ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
}

li {
  float: left;
  border-right:1px solid #bbb;
}

li:last-child {
  border-right: none;
}

li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover:not(.active) {
  background-color: #111;
}

.active {
  background-color: #4CAF50;
}
	</style>
</head>
<body style= 'background:  #45e606'>
<center>
    <div style="background: #40bd0f">	
    <img src="apotik.png" alt="Gambar Bolu" width="200" height="200" style="border-radius: 50%;">	
        <h1>Apotik Kurnia Jaya</h1>
		<h2>Jalan Palembang-Jambi km.102, Sungai Lilin, Musi Banyuasin, Sumatera Selatan</h2>
	</div>
	<br/>
    <ul>
  <li><a class="tombol" href="in_penj.php">+ Tambah Data Penjualan Baru</a>
  <li><a class="tombol" href="lihat_penjualan.php">+ Lihat Pendapatan</a>
  <li style="float:right"><a class="tombol" href="index.php">+ Lihat Data Obat</a>
</ul>
	<br/>
	
 
	<table border="1" class="table">
		<tr>
			
            <th>Kode Transaksi</th>
			<th>Id Pelanggan</th>
			<th>Id Obat</th>
			<th>Tanggal Transaksi</th>
			<th>jumlah</th>	
            <th>Total Harga</th>		
		</tr>
        
        <?php
            foreach($resultss as $resultt) :
        ?>
		<tr>

			<td><?php echo $resultt['kode_transaksi']; ?></td>
            <td><?php echo $resultt['id_pelanggan']; ?></td>
			<td><?php echo $resultt['id_obat']; ?></td>
			<td><?php echo $resultt['tgl_transaksi']; ?></td>
			<td><?php echo $resultt['jumlah']; ?></td>
            <td><?php echo $resultt['total_harga']; ?></td>

		</tr>
		
        <?php
            endforeach;
        ?>
	</table>
    <br><br>
    <br><br>
    <br><br>
    </center>
</body>
</html>
