<?php

    require_once("koneksi.php");
    $sql_get = "SELECT * FROM obat";
    $query_pj = mysqli_query($koneksi, $sql_get);
    $results = [];
    while($row = mysqli_fetch_assoc($query_pj)){
        $results[] = $row;
    }

?>

<!DOCTYPE html>
<html>
<head>
	<title>TP Basis Data</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<style>
		a:link, a:visited {
  		background-color: #0fbda0;
  		color: white;
  		padding: 5px 3px;
  		text-align: center;
  		text-decoration: none;
  		display: inline-block;
		}

		a:hover, a:active {
  		background-color: red;
		}
		th{
		padding: 15px 3px;
		background-color: #0fbda0;
		text-align: center;
  		text-decoration: none;
		 }
		 ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
}

li {
  float: left;
  border-right:1px solid #bbb;
}

li:last-child {
  border-right: none;
}

li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover:not(.active) {
  background-color: #111;
}

.active {
  background-color: #4CAF50;
}
	</style>
</head>
<body style= 'background:  #45e606'>
<center>
	<div style="background: #40bd0f">		
		 <img src="apotik.png" alt="Gambar Bolu" width="200" height="200" style="border-radius: 50%;">
		<h1>Apotik Kurnia Jaya</h1>
		<h2>Jalan Palembang-Jambi km.102, Sungai Lilin, Musi Banyuasin, Sumatera Selatan</h2>
	</div>
	<br/>
	<ul>
  <li><a  href="input.php">+ Tambah Data Baru</a>
  <li style="float:right"><a  href="penjualan.php">+ Lihat Data Penjualan</a>
</ul>
	<br/>

	<table border="1" class="table">
		<tr>	
            <th>Id Obat</th>
			<th>Nama Obat</th>
			<th>Jenis Obat</th>
			<th>Stock</th>
			<th>Harga</th>	
            <th>Opsi</th>		
		</tr>
        
        <?php
            foreach($results as $result) :
        ?>
		<tr>
			
            <td><?php echo $result['id_obat']; ?></td>
			<td><?php echo $result['nama_obat']; ?></td>
			<td><?php echo $result['jenis_obat']; ?></td>
			<td><?php echo $result['stock']; ?></td>
            <td><?php echo $result['harga']; ?></td>
			<td>
				<a href="edit.php?id_obat=<?=$result['id_obat'];?>">Edit</a> ||
				<a href="hapus.php?id_obat=<?=$result['id_obat'];?>">Hapus</a>					
			</td>
		</tr>
		
        <?php
            endforeach;
        ?>
	</table>
    <br><br>
	<br><br>
    </center>
</body>
</html>
